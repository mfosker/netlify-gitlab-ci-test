import React from 'react';
import logo from './mobforwards-logo-negative2.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <a          
          href="https://www.youtube.com/watch?v=w4llHcz-tzg"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={logo} className="App-logo" alt="#mobforwards #nocodereviews Some tiny change" />
        </a>
      </header>
    </div>
  );
}

export default App;
